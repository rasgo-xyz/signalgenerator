#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QElapsedTimer>
#include <QMap>

namespace Ui {
class MainWindow;
}

class QCustomPlot;
class pSignalGenerator;
class RTPlot;
class Waveform;

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

public slots:
    void handleStart();
    void handleStop();
    void handleFrequencyChanged(int freq);
    void handleSamplingFrequencyChanged(int sampFreq);
    void handleTimeWindowChanged(int timeWindow);
    void handleNewSample(double data);

private:
    void _setupConnections();

    Ui::MainWindow *ui;

    RTPlot *m_rtplot;
    QMap<pSignalGenerator*,Waveform*> m_sigWaveformMapper;
};

#endif // MAINWINDOW_H
