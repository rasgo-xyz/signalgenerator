#-------------------------------------------------
#
# Project created by QtCreator 2013-10-28T17:06:42
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets printsupport

TARGET = pSignalGenerator
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    psignalgenerator.cpp \
    qcustomplot.cpp \
    rtplot.cpp

HEADERS  += mainwindow.h \
    psignalgenerator.h \
    qcustomplot.h \
    rtplot.h

FORMS    += mainwindow.ui

CONFIG(debug, debug|release) {
    DESTDIR = debug
} else {
    DESTDIR = release
}

OBJECTS_DIR = build/obj
MOC_DIR = build/moc
RCC_DIR = build/rcc
UI_DIR = build/ui
