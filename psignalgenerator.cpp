#include "psignalgenerator.h"

#include <QDebug>

#include <QTimer>
#include <QElapsedTimer>
#include <QDateTime>
#include <QtCore/QtMath>

pSignalGenerator::pSignalGenerator(const QString &name, QObject *parent) :
    QObject(parent)
{
    m_name = name;
    m_timer = new QTimer(this);
    m_timer->setTimerType(Qt::PreciseTimer);

    m_waveformType = wtSine;
    m_amplitude = 1.0;
    m_offset = 0.0;
    m_frequency = 1.0;
    m_sampFreq = 100.0;

    connect(m_timer, SIGNAL(timeout()), this, SLOT(_sample()));
}

void pSignalGenerator::setWaveformType(WaveformType wt)
{
    m_waveformType = wt;
}

void pSignalGenerator::setProprieties(double amplitude, double offset, double frequency)
{
    m_amplitude = amplitude;
    m_offset = offset;
    m_frequency = frequency;
}

void pSignalGenerator::setSamplingFrequency(double frequency)
{
    m_sampFreq = frequency;
    m_timer->setInterval((int)(1000.0/frequency));
}

void pSignalGenerator::setSignalOffset(double offset)
{
    m_offset = offset;
}

void pSignalGenerator::setSignalAmplitude(double amplitude)
{
    m_amplitude = amplitude;
}

void pSignalGenerator::setSignalFrequency(double frequency)
{
    m_frequency = frequency;
}

void pSignalGenerator::_sample()
{
    double time = (double)m_clock.elapsed()/1000.0; //QDateTime::currentDateTime().toMSecsSinceEpoch()/1000.0;
    double value;
    switch(m_waveformType)
    {
    case wtTriangle:
        value = m_offset+m_amplitude*cos(2.0*M_PI*m_frequency*time); //sin(key*1.6+cos(key*1.7)*2)*10 + sin(key*1.2+0.56)*20 + 26;
        break;
    case wtSine:
        value = m_offset+m_amplitude*sin(2.0*M_PI*m_frequency*time); //sin(key*1.6+cos(key*1.7)*2)*10 + sin(key*1.2+0.56)*20 + 26;
        break;
    }

    //qDebug() << time << value;
    emit newSample(value);
}

void pSignalGenerator::start()
{
    m_timer->setInterval((int)(1000.0/m_sampFreq));
    m_clock.restart();
    _sample();
    m_timer->start();
}

void pSignalGenerator::stop()
{
    m_timer->stop();
}

