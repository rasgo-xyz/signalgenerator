#ifndef PSIGNALGENERATOR_H
#define PSIGNALGENERATOR_H

#include <QObject>
class QTimer;
#include <QElapsedTimer>

class pSignalGenerator : public QObject
{
    Q_OBJECT
public:
    enum WaveformType
    {
        wtSquare,
        wtSine,
        wtTriangle,
        wtSawtooth
    };

    explicit pSignalGenerator(const QString &name = QString(), QObject *parent = 0);
    void setWaveformType(WaveformType wt);
    void setProprieties(double amplitude, double offset, double frequency);
signals:
    void newSample(double value);

public slots:
    void setSamplingFrequency(double frequency);
    void setSignalAmplitude(double amplitude);
    void setSignalOffset(double offset);
    void setSignalFrequency(double frequency);
    void start();
    void stop();

private slots:
    void _sample();

private:
    QString m_name;
    double m_amplitude;
    double m_offset;
    double m_frequency;
    double m_sampFreq;
    WaveformType m_waveformType;
    QTimer *m_timer;
    QElapsedTimer m_clock;

};

#endif // PSIGNALGENERATOR_H
