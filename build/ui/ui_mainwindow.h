/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.1.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDial>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>
#include "rtplot.h"

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QWidget *centralWidget;
    QVBoxLayout *verticalLayout_7;
    QVBoxLayout *verticalLayout_5;
    RTPlot *plotWidget;
    QHBoxLayout *horizontalLayout;
    QVBoxLayout *verticalLayout_4;
    QLabel *label_2;
    QDial *offset_dial;
    QSpinBox *spinBox;
    QVBoxLayout *verticalLayout_3;
    QLabel *label;
    QDial *amplitude_dial;
    QSpinBox *spinBox_2;
    QVBoxLayout *verticalLayout_2;
    QLabel *label_3;
    QDial *frequency_dial;
    QSpinBox *frequency_spin;
    QVBoxLayout *verticalLayout_6;
    QLabel *label_4;
    QSpinBox *sampFreq_spin;
    QLabel *label_5;
    QSpinBox *timeWindow_spin;
    QVBoxLayout *verticalLayout;
    QPushButton *start_button;
    QPushButton *stop_button;
    QMenuBar *menuBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QStringLiteral("MainWindow"));
        MainWindow->resize(466, 384);
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        verticalLayout_7 = new QVBoxLayout(centralWidget);
        verticalLayout_7->setSpacing(6);
        verticalLayout_7->setContentsMargins(11, 11, 11, 11);
        verticalLayout_7->setObjectName(QStringLiteral("verticalLayout_7"));
        verticalLayout_5 = new QVBoxLayout();
        verticalLayout_5->setSpacing(6);
        verticalLayout_5->setObjectName(QStringLiteral("verticalLayout_5"));
        plotWidget = new RTPlot(centralWidget);
        plotWidget->setObjectName(QStringLiteral("plotWidget"));
        QSizePolicy sizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(1);
        sizePolicy.setHeightForWidth(plotWidget->sizePolicy().hasHeightForWidth());
        plotWidget->setSizePolicy(sizePolicy);

        verticalLayout_5->addWidget(plotWidget);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setSpacing(6);
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        verticalLayout_4 = new QVBoxLayout();
        verticalLayout_4->setSpacing(6);
        verticalLayout_4->setObjectName(QStringLiteral("verticalLayout_4"));
        label_2 = new QLabel(centralWidget);
        label_2->setObjectName(QStringLiteral("label_2"));
        QSizePolicy sizePolicy1(QSizePolicy::Preferred, QSizePolicy::Fixed);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(label_2->sizePolicy().hasHeightForWidth());
        label_2->setSizePolicy(sizePolicy1);
        label_2->setAlignment(Qt::AlignCenter);

        verticalLayout_4->addWidget(label_2);

        offset_dial = new QDial(centralWidget);
        offset_dial->setObjectName(QStringLiteral("offset_dial"));
        sizePolicy1.setHeightForWidth(offset_dial->sizePolicy().hasHeightForWidth());
        offset_dial->setSizePolicy(sizePolicy1);
        offset_dial->setMinimumSize(QSize(0, 50));
        offset_dial->setMaximumSize(QSize(16777215, 50));
        offset_dial->setMinimum(1);
        offset_dial->setMaximum(10);
        offset_dial->setPageStep(2);
        offset_dial->setTracking(true);
        offset_dial->setOrientation(Qt::Horizontal);
        offset_dial->setWrapping(false);
        offset_dial->setNotchesVisible(true);

        verticalLayout_4->addWidget(offset_dial);

        spinBox = new QSpinBox(centralWidget);
        spinBox->setObjectName(QStringLiteral("spinBox"));
        spinBox->setMaximumSize(QSize(80, 16777215));

        verticalLayout_4->addWidget(spinBox);


        horizontalLayout->addLayout(verticalLayout_4);

        verticalLayout_3 = new QVBoxLayout();
        verticalLayout_3->setSpacing(6);
        verticalLayout_3->setObjectName(QStringLiteral("verticalLayout_3"));
        label = new QLabel(centralWidget);
        label->setObjectName(QStringLiteral("label"));
        sizePolicy1.setHeightForWidth(label->sizePolicy().hasHeightForWidth());
        label->setSizePolicy(sizePolicy1);
        label->setAlignment(Qt::AlignCenter);

        verticalLayout_3->addWidget(label);

        amplitude_dial = new QDial(centralWidget);
        amplitude_dial->setObjectName(QStringLiteral("amplitude_dial"));
        sizePolicy1.setHeightForWidth(amplitude_dial->sizePolicy().hasHeightForWidth());
        amplitude_dial->setSizePolicy(sizePolicy1);
        amplitude_dial->setMinimumSize(QSize(0, 50));
        amplitude_dial->setMaximumSize(QSize(16777215, 50));
        amplitude_dial->setMinimum(1);
        amplitude_dial->setMaximum(10);
        amplitude_dial->setPageStep(2);
        amplitude_dial->setNotchesVisible(true);

        verticalLayout_3->addWidget(amplitude_dial);

        spinBox_2 = new QSpinBox(centralWidget);
        spinBox_2->setObjectName(QStringLiteral("spinBox_2"));
        spinBox_2->setMaximumSize(QSize(80, 16777215));

        verticalLayout_3->addWidget(spinBox_2);


        horizontalLayout->addLayout(verticalLayout_3);

        verticalLayout_2 = new QVBoxLayout();
        verticalLayout_2->setSpacing(6);
        verticalLayout_2->setObjectName(QStringLiteral("verticalLayout_2"));
        label_3 = new QLabel(centralWidget);
        label_3->setObjectName(QStringLiteral("label_3"));
        sizePolicy1.setHeightForWidth(label_3->sizePolicy().hasHeightForWidth());
        label_3->setSizePolicy(sizePolicy1);
        label_3->setAlignment(Qt::AlignCenter);

        verticalLayout_2->addWidget(label_3);

        frequency_dial = new QDial(centralWidget);
        frequency_dial->setObjectName(QStringLiteral("frequency_dial"));
        frequency_dial->setMinimumSize(QSize(0, 50));
        frequency_dial->setMaximumSize(QSize(16777215, 50));
        frequency_dial->setMinimum(1);
        frequency_dial->setMaximum(10);
        frequency_dial->setPageStep(2);
        frequency_dial->setOrientation(Qt::Horizontal);
        frequency_dial->setInvertedAppearance(false);
        frequency_dial->setInvertedControls(false);
        frequency_dial->setWrapping(false);
        frequency_dial->setNotchesVisible(true);

        verticalLayout_2->addWidget(frequency_dial);

        frequency_spin = new QSpinBox(centralWidget);
        frequency_spin->setObjectName(QStringLiteral("frequency_spin"));
        frequency_spin->setMaximumSize(QSize(80, 16777215));
        frequency_spin->setReadOnly(true);
        frequency_spin->setMinimum(1);
        frequency_spin->setMaximum(10);

        verticalLayout_2->addWidget(frequency_spin);


        horizontalLayout->addLayout(verticalLayout_2);

        verticalLayout_6 = new QVBoxLayout();
        verticalLayout_6->setSpacing(6);
        verticalLayout_6->setObjectName(QStringLiteral("verticalLayout_6"));
        label_4 = new QLabel(centralWidget);
        label_4->setObjectName(QStringLiteral("label_4"));

        verticalLayout_6->addWidget(label_4);

        sampFreq_spin = new QSpinBox(centralWidget);
        sampFreq_spin->setObjectName(QStringLiteral("sampFreq_spin"));
        sampFreq_spin->setMinimum(1);

        verticalLayout_6->addWidget(sampFreq_spin);

        label_5 = new QLabel(centralWidget);
        label_5->setObjectName(QStringLiteral("label_5"));

        verticalLayout_6->addWidget(label_5);

        timeWindow_spin = new QSpinBox(centralWidget);
        timeWindow_spin->setObjectName(QStringLiteral("timeWindow_spin"));
        timeWindow_spin->setMinimum(1);
        timeWindow_spin->setMaximum(60);

        verticalLayout_6->addWidget(timeWindow_spin);


        horizontalLayout->addLayout(verticalLayout_6);

        verticalLayout = new QVBoxLayout();
        verticalLayout->setSpacing(6);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        start_button = new QPushButton(centralWidget);
        start_button->setObjectName(QStringLiteral("start_button"));
        QSizePolicy sizePolicy2(QSizePolicy::Expanding, QSizePolicy::Expanding);
        sizePolicy2.setHorizontalStretch(0);
        sizePolicy2.setVerticalStretch(0);
        sizePolicy2.setHeightForWidth(start_button->sizePolicy().hasHeightForWidth());
        start_button->setSizePolicy(sizePolicy2);
        start_button->setDefault(false);
        start_button->setFlat(false);

        verticalLayout->addWidget(start_button);

        stop_button = new QPushButton(centralWidget);
        stop_button->setObjectName(QStringLiteral("stop_button"));
        sizePolicy2.setHeightForWidth(stop_button->sizePolicy().hasHeightForWidth());
        stop_button->setSizePolicy(sizePolicy2);

        verticalLayout->addWidget(stop_button);


        horizontalLayout->addLayout(verticalLayout);


        verticalLayout_5->addLayout(horizontalLayout);


        verticalLayout_7->addLayout(verticalLayout_5);

        MainWindow->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(MainWindow);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 466, 20));
        MainWindow->setMenuBar(menuBar);
        statusBar = new QStatusBar(MainWindow);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        MainWindow->setStatusBar(statusBar);

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "MainWindow", 0));
        label_2->setText(QApplication::translate("MainWindow", "Offset", 0));
        label->setText(QApplication::translate("MainWindow", "Amplitude", 0));
        label_3->setText(QApplication::translate("MainWindow", "Frequency", 0));
        label_4->setText(QApplication::translate("MainWindow", "Sampling frequency", 0));
        label_5->setText(QApplication::translate("MainWindow", "Time window (sec)", 0));
        start_button->setText(QApplication::translate("MainWindow", "Start", 0));
        stop_button->setText(QApplication::translate("MainWindow", "Stop", 0));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
