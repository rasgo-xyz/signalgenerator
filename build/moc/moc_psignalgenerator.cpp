/****************************************************************************
** Meta object code from reading C++ file 'psignalgenerator.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.1.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../psignalgenerator.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'psignalgenerator.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.1.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_pSignalGenerator_t {
    QByteArrayData data[14];
    char stringdata[156];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    offsetof(qt_meta_stringdata_pSignalGenerator_t, stringdata) + ofs \
        - idx * sizeof(QByteArrayData) \
    )
static const qt_meta_stringdata_pSignalGenerator_t qt_meta_stringdata_pSignalGenerator = {
    {
QT_MOC_LITERAL(0, 0, 16),
QT_MOC_LITERAL(1, 17, 9),
QT_MOC_LITERAL(2, 27, 0),
QT_MOC_LITERAL(3, 28, 5),
QT_MOC_LITERAL(4, 34, 20),
QT_MOC_LITERAL(5, 55, 9),
QT_MOC_LITERAL(6, 65, 18),
QT_MOC_LITERAL(7, 84, 9),
QT_MOC_LITERAL(8, 94, 15),
QT_MOC_LITERAL(9, 110, 6),
QT_MOC_LITERAL(10, 117, 18),
QT_MOC_LITERAL(11, 136, 5),
QT_MOC_LITERAL(12, 142, 4),
QT_MOC_LITERAL(13, 147, 7)
    },
    "pSignalGenerator\0newSample\0\0value\0"
    "setSamplingFrequency\0frequency\0"
    "setSignalAmplitude\0amplitude\0"
    "setSignalOffset\0offset\0setSignalFrequency\0"
    "start\0stop\0_sample\0"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_pSignalGenerator[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       8,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       1,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    1,   54,    2, 0x05,

 // slots: name, argc, parameters, tag, flags
       4,    1,   57,    2, 0x0a,
       6,    1,   60,    2, 0x0a,
       8,    1,   63,    2, 0x0a,
      10,    1,   66,    2, 0x0a,
      11,    0,   69,    2, 0x0a,
      12,    0,   70,    2, 0x0a,
      13,    0,   71,    2, 0x08,

 // signals: parameters
    QMetaType::Void, QMetaType::Double,    3,

 // slots: parameters
    QMetaType::Void, QMetaType::Double,    5,
    QMetaType::Void, QMetaType::Double,    7,
    QMetaType::Void, QMetaType::Double,    9,
    QMetaType::Void, QMetaType::Double,    5,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void pSignalGenerator::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        pSignalGenerator *_t = static_cast<pSignalGenerator *>(_o);
        switch (_id) {
        case 0: _t->newSample((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 1: _t->setSamplingFrequency((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 2: _t->setSignalAmplitude((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 3: _t->setSignalOffset((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 4: _t->setSignalFrequency((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 5: _t->start(); break;
        case 6: _t->stop(); break;
        case 7: _t->_sample(); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        void **func = reinterpret_cast<void **>(_a[1]);
        {
            typedef void (pSignalGenerator::*_t)(double );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&pSignalGenerator::newSample)) {
                *result = 0;
            }
        }
    }
}

const QMetaObject pSignalGenerator::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_pSignalGenerator.data,
      qt_meta_data_pSignalGenerator,  qt_static_metacall, 0, 0}
};


const QMetaObject *pSignalGenerator::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *pSignalGenerator::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_pSignalGenerator.stringdata))
        return static_cast<void*>(const_cast< pSignalGenerator*>(this));
    return QObject::qt_metacast(_clname);
}

int pSignalGenerator::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 8)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 8;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 8)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 8;
    }
    return _id;
}

// SIGNAL 0
void pSignalGenerator::newSample(double _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}
QT_END_MOC_NAMESPACE
