#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "psignalgenerator.h"
#include "qcustomplot.h"

#include <QDebug>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    m_rtplot = ui->plotWidget;

    pSignalGenerator *sine = new pSignalGenerator("sin()");
    sine->setWaveformType(pSignalGenerator::wtSine);

    pSignalGenerator *cosine = new pSignalGenerator("cos()");
    cosine->setWaveformType(pSignalGenerator::wtTriangle);
    cosine->setSamplingFrequency(10);
    cosine->setSignalAmplitude(0.5);

    m_sigWaveformMapper.insert(sine, m_rtplot->addWaveform());
    m_sigWaveformMapper.insert(cosine, m_rtplot->addWaveform());

    connect(sine, SIGNAL(newSample(double)), this, SLOT(handleNewSample(double)));
    connect(cosine, SIGNAL(newSample(double)), this, SLOT(handleNewSample(double)));

    _setupConnections();

    ui->timeWindow_spin->setValue(m_rtplot->timeWindow());
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::_setupConnections()
{
    connect(ui->start_button, SIGNAL(clicked()), this, SLOT(handleStart()));
    connect(ui->stop_button, SIGNAL(clicked()), this, SLOT(handleStop()));
    connect(ui->frequency_dial, SIGNAL(valueChanged(int)), this, SLOT(handleFrequencyChanged(int)));

    connect(ui->timeWindow_spin, SIGNAL(valueChanged(int)), this, SLOT(handleTimeWindowChanged(int)));
}

void MainWindow::handleStart()
{
    m_rtplot->start();
    QList<pSignalGenerator*> sigGens = m_sigWaveformMapper.keys();
    foreach(pSignalGenerator *sigGen, sigGens)
        sigGen->start();
}

void MainWindow::handleStop()
{
    QList<pSignalGenerator*> sigGens = m_sigWaveformMapper.keys();
    foreach(pSignalGenerator *sigGen, sigGens)
        sigGen->stop();
    m_rtplot->stop();
}

void MainWindow::handleFrequencyChanged(int freq)
{
    ui->frequency_spin->setValue(freq);
}

void MainWindow::handleSamplingFrequencyChanged(int sampFreq)
{

}

void MainWindow::handleTimeWindowChanged(int timeWindow)
{
    m_rtplot->setTimeWindow(timeWindow);
}

void MainWindow::handleNewSample(double data)
{
    pSignalGenerator *sigGen = (pSignalGenerator*)sender();
    m_rtplot->addData(data, m_sigWaveformMapper.value(sigGen));
    m_rtplot->replot();
}

